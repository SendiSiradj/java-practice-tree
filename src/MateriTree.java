public class MateriTree {
    Node root;

    MateriTree(int key){
        root = new Node(key);
    }

    MateriTree(){
        root = null;
    }

    void printPath(Node node) {
        int[] path = new int[1000];
        printPathRecursion(node, path, 0);
    }

    void printPathRecursion(Node node, int[] path, int pathLength){
        if(node == null){
            return;
        }
        path[pathLength] = node.key;
        pathLength++;

        if(node.left == null && node.right == null){
            printArray(path, pathLength);
        }else{
            printPathRecursion(node.left, path, pathLength);
            printPathRecursion(node.right, path, pathLength);
        }
    }
    void printArray(int[] path, int pathLength){
        for(int i = 0; i < pathLength; i++){
            System.out.print(path[i]+ " ");
        }
        System.out.print("");
    }

    public static void main(String[] args) {
        MateriTree pohon = new MateriTree(10);
        pohon.root.left = new Node(9);
        pohon.root.right = new Node(11);

        pohon.root.left.left = new Node(4);
        pohon.root.left.right = new Node(5);

        pohon.printPath(pohon.root);

    }
}

public class Mobil {

    String nama;
    int kecepatan;

    Mobil(String nama, int kecepatan){
        this.nama = nama;
        this.kecepatan = kecepatan;
    }

    public int getKecepatan() {
        return kecepatan;
    }

    public String getNama() {
        return nama;
    }
}

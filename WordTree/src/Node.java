public class Node {

    String key;
    Mobil infomobil;
    Node left, right;

    Node(String key){
        this.key = key;
        left = right = null;
    }
    
    Node(Mobil infomobil){
        this.infomobil = infomobil;
        left = right = null;
    }

}

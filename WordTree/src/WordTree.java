public class WordTree {

    Node root;

    WordTree(String key){
        root = new Node(key);
    }

    WordTree(){
        root = null;
    }
    WordTree(Mobil mobil){
        root = new Node(mobil); // membuat Node pertama, left dan right isinya null

    }

    void printPath(Node node) {
        Mobil[] path = new Mobil[1000];
        printPathRecursion(node, path, 0);
    }

    void printPathRecursion(Node node, Mobil[] path, int pathLength){
        if(node == null){
            return; // keluar dari function
        }
        path[pathLength] = node.infomobil;
        pathLength++;

        if(node.left == null && node.right == null){
            printArray(path, pathLength);
        }else{
            printPathRecursion(node.left, path, pathLength);
            printPathRecursion(node.right, path, pathLength);
        }
    }

    void printArray(Mobil[] path, int pathLength){
        for(int i = 0; i < pathLength; i++){
            System.out.print(path[i].nama + "," +path[i].kecepatan +"->");
        }
        System.out.println();
    }






    public static void main(String[] args) {
        /*WordTree pohon = new WordTree("Root");
        pohon.root.left = new Node("Left1");
        pohon.root.right = new Node("Right1");

        pohon.root.left.left = new Node("Left2");
        pohon.root.left.right = new Node("Left1_Right1");

        pohon.printPath(pohon.root);

        System.out.println("--------------------------------");
*/

        WordTree kumpulanMobil = new WordTree(new Mobil("Mazda RX8", 200));
        kumpulanMobil.root.left = new Node(new Mobil("Toyota Rush", 80));

        kumpulanMobil.printPath(kumpulanMobil.root);

    }
}
